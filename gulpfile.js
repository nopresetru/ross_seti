var
  	gulp 			    = require('gulp'),
	browsersync		= require('browser-sync'),
	notify 			  = require("gulp-notify"),
	plumber 	   	= require('gulp-plumber'),

	sass        	= require('gulp-sass'),
	sourcemaps 		= require('gulp-sourcemaps'),
	minifyCss 		= require('gulp-clean-css'),

	pug 		    	= require('gulp-pug'),
	prettify 	  	= require('gulp-prettify'),

	concat 		  	= require('gulp-concat'),
	uglify 		  	= require('gulp-uglify'),
	rename 			  = require("gulp-rename"),
	autoprefixer	= require('gulp-autoprefixer'),
	runSequence		= require('run-sequence'),

	overrideBrowserslist	= ['ie 11', 'last 2 versions'],
	reload 			= browsersync.reload;

	var configPrettify = {
		indent_char: '\t',
		indent_size: 1,
		indent_inner_html: true,
		unformatted: []
	};

	var configPlumber = {
		errorHandler: notify.onError("\n<%= error.message %>")
	};

// pug
// ---------------------------------------------------------------------------------

gulp.task('pages', function() {
  return gulp.src('./app/pages/*.pug')
    .pipe(plumber(configPlumber))
    .pipe( pug())
    .pipe(pug({pretty: true}))
    .pipe(prettify(configPrettify))
    .pipe( gulp.dest('./public'))

});

// sass
// ---------------------------------------------------------------------------------

gulp.task('sass', function() {
  return gulp.src('./app/scss/main.scss')
  .pipe(plumber(configPlumber))
  .pipe(sass())
  .pipe(autoprefixer({ overrideBrowserslis: ['last 2 versions'] }))
  .pipe(minifyCss({
    advanced: true,
    restructuring: false,
    keepBreaks: false,
  }))
  .pipe(rename('template_styles.css'))
  .pipe( gulp.dest('./public/tpl'))
  .pipe(reload({stream:true}));

});


// js
// ---------------------------------------------------------------------------------

gulp.task('js', function() {
    return gulp.src(['app/js/*.js'])
        .pipe(concat('lib.min.js'))
		.pipe(uglify())
        .pipe(gulp.dest('public/tpl/js'))
		.pipe(reload({stream:true}));

})


//

gulp.task('copy', function() {
	gulp.src("./app/static/**")
		.pipe(gulp.dest('public/tpl'))
});




// autoprefixer
// ---------------------------------------------------------------------------------

gulp.task('autoprefixer', function () {
    var postcss      = require('gulp-postcss');
    var sourcemaps   = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');

    return gulp.src('./public/*.css')
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({ overrideBrowserslis: ['last 2 versions'] }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./public'));
});

gulp.task('default', ['pages', 'sass', 'js', 'copy'], function () {

	browsersync({
		server: {
			baseDir: "public",
			directory: true
		}
	});

	gulp.watch(['./app/scss/**/*.scss'], ['sass']);

	gulp.watch(['./app/pages/**/*.pug'], function(){ runSequence('pages', reload)});

	gulp.watch(['./app/js/*.js'], ['js']);

  gulp.watch("app/static/**/*", function() { runSequence('copy', reload) });

});

gulp.task('build', ['pages', 'sass', 'js', 'copy'])
